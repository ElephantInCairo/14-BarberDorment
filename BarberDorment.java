/* Classe principal BarberDorment, Activitat: BarberDorment, José Sánchez Moreno 16/11/15 */

import java.util.concurrent.Semaphore;
import java.util.Random;

public class BarberDorment implements Runnable {

    private static final int nClients = 20;
    private static final int nCadires = 5;
    private final Buffer<Consumible> coa;
    private final Barber b;

    /*
     * Per una barberia és necessàri un barber
     *  i una coa d'aquest barber
     *
     * nCadires: nombre de gent que pot seure i
     *  esperar el seu torn
     *
     * nClients: nombre de gent que vol atendre
     *  el barber abans de tancar la barberia
     */
    public BarberDorment()
    {
	b = new Barber(nCadires, nClients);
	coa = b.getCoa();
    }

    @Override
    public void run()
    {
	//arranca el barber
	Thread tb = new Thread(b); tb.start();

	//fes venir clients mentres estigui obert
	Thread tc;
	Random rnd = new Random();
	while ( coa.obert() )
	{
	    //temps entre client i client
	    try {
		Thread.sleep(rnd.nextInt(3500));
	    } catch (InterruptedException e) {}
	    tc = new Thread( new Client(coa) ); tc.start();
	}

	System.out.println("El barber tanca la barberia");

	//que passa si entren quan està tancat
	tc = new Thread( new Client(coa) ); tc.start();
	tc = new Thread( new Client(coa) ); tc.start();
    }

    public static void main(String[] args) throws InterruptedException
    {
	BarberDorment barberia = new BarberDorment();
	barberia.run();
    }
}
