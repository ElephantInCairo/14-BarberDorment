/* Classe Buffer, Activitat: BarberDorment, José Sánchez Moreno 16/11/15*/

import java.util.concurrent.Semaphore;

/* Classe Buffer, estructura de dades que ens
 *  gestiona Cosumibles amb semàfors
 *
 * Només un procés consumeix del búffer (barber)
 *  s'assumeix que no necessitam gestionar que
 *  hi hagui varis consumidors. Basta gestionar
 *  la concurrència on es preveu que hi haurà
 *  competència (al afegir al búffer).
 *
 */

public class Buffer<Item extends Consumible> {

    //talla cabells de un en un
    private final Semaphore torn;
    //inserció a la coa ha de ser atòmica
    private final Semaphore exclMut;
    //gestionen la gent que hi cap a la coa
    private final Semaphore noBuid;
    private final Semaphore noPle;
    //gestiona el límit de clients per atendre
    private final Semaphore counter;
    private final String MSG;

    public Buffer(int max, int limit, String msg)
    {
	torn = new Semaphore(1);
	exclMut = new Semaphore(1);
	noBuid  = new Semaphore(0);
	noPle   = new Semaphore(max);
	counter = new Semaphore(limit);
	MSG = msg;
    }

    /*
     * La acció de consumir es satisfà completament
     *  al retorn de la funció.
     *
     * El consumidor només necessita saber l'estat
     *  del búffer per saber si pot o no dormir, però
     *  sempre que consumeix acaba consumint encara
     *  que hagui d'esperar. El consumidor decideix
     *  que fer en funció de l'estat del búffer.
     *
     */
    public void consumeix(Actor consumidor)
    {
	consumidor.decideix(noBuid.availablePermits());

	//agafa un client de la coa
	try {
	    noBuid.acquire();
	} catch (InterruptedException e) {}

	noPle.release();

	consumidor.accio();

	//deixa lliure per tallar els cabells
	torn.release();

    }

    /*
     * Es podria retornar un codi de "perque fals"
     *  i gestionar-ho després de la cridada però
     *  no ho veig massa "correcte". Crec que així
     *  és la pròpia coa/buffer qui gestiona els
     *  events que ocórren.
     *
     * El búffer no espera a que hi hagui lloc
     *  per fer la inserció, d'aquesta manera
     *  s'ha de notificar al producte que no
     *  ha estat inserit.
     *
     * El búffer es pot bloquejar al arribar a
     *  cert límit d'elements (per entrar, no
     *  per consumir).
     *
     * El productor (Item) necessita saber l'estat
     *  del búffer per saber si ha de continuar amb
     *  l'operació o si prefereix no continuar.
     *
     * La coa en aquest cas és la que no permet
     *  esperar per entrar, quan està bloquejada
     *  perque està plena, directament no permet
     *  ni inserir ni que l'alliberi el semàfor
     *  de plena (en tot cas, l'Item podria decidir
     *  reintentar entrar).
     *
     */
    public boolean afegeix( Item x )
    {
	//Afegir a la coa de la barberia
	try {
	    exclMut.acquire();
	} catch (InterruptedException e) {}

	    //búffer bloquejat per límit
	    if (counter.availablePermits() == 0)
	    {
		System.out.println(MSG);
		exclMut.release();
		return false;
	    }

	    //búffer bloquejat perque està ple
	    if (noPle.availablePermits() == 0)
	    {
		x.desbordament();
		exclMut.release();
		return false;
	    }

	try {
	    noPle.acquire();
	    counter.acquire();
	} catch (InterruptedException e) {}

	x.eventEntrada();
	
	noBuid.release();
	exclMut.release();
	//fi afegir a la coa de la barberia
	
	//entr a que me tallin els cabells
	try {
	    torn.acquire();
	} catch (InterruptedException e) {}

	return true;
    }

    /*
     * Indica quan és que la barberia està
     *  tancada i, per tant, ja no accepta
     *  pus Clients
     */
    public boolean obert()
    {
	return counter.availablePermits() != 0;
    }

    public boolean buida()
    {
	return noBuid.availablePermits() == 0;
    }
}
