/* Interface Consumible, Activitat: BarberDorment, José Sánchez Moreno 16/11/15 */

interface Consumible {
    void desbordament();
    void eventEntrada();
}
