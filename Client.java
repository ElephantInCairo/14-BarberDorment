/* Classe Client, Activitat: BarberDorment, José Sánchez Moreno 16/11/15 */

public class Client implements Consumible, Runnable {

    //Manté el reconte de les instàncies
    private static int counter = 0;
    private final Buffer<Consumible> coa;
    private final String nom;

    public Client(Buffer<Consumible> b)
    {
	nom = "Client "+ ++counter;
	coa = b;
    }

    /*
     * Acció del client quan sap que serà atès
     */
    @Override
    public void eventEntrada()
    {
	System.out.println("\t\t"+ this +" espera");
    }

    /*
     * Acció del client quan no te lloc a
     *  la barberia
     */
    @Override
    public void desbordament()
    {
	System.out.println("\t\t"+ this +" no troba lloc a la coa i se'n va");
    }

    @Override
    public void run()
    {
	if ( coa.afegeix(this) )
	{
	    //espera mentres li tallen els cabells
	    // (mateix temps)
	    try {
		Thread.sleep(3000);
	    } catch (InterruptedException e) {}
	    System.out.println("\t\t"+ this +" se'n va amb els cabells tallats");
	}
    }

    @Override
    public String toString()
    {
	return nom;
    }
}
