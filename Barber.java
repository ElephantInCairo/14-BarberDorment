/* Classe Barber, Activitat: BarberDorment, José Sánchez Moreno 16/11/15 */

public class Barber implements Actor, Runnable {

    private final Buffer<Consumible> coa;
    private int clientsAtesos;

    public Barber(int nCadires, int nClients)
    {
	clientsAtesos = 0;
	coa = new Buffer<Consumible>(nCadires, nClients,
		"La barberia està tancada");
    }

    public Buffer<Consumible> getCoa()
    {
	return coa;
    }

    /*
     * Decisió que pren el barber quan veu
     *  la longitud del búffer
     */
    @Override
    public void decideix(int nItems)
    {
	if ( nItems == 0 )
	    System.out.println("El barber s'adorm");
    }

    @Override
    public void accio()
    {
	System.out.println("El barber tallant els cabells, nombre de clients "+ ++clientsAtesos);
	try {
	    Thread.sleep(3000);
	} catch (InterruptedException e) {}
    }

    /*
     * Mentres entren clients O hi ha clients,
     *  despatxar-los
     */
    @Override
    public void run()
    {
	while ( coa.obert() || !coa.buida() )
	    coa.consumeix(this);

	System.out.println("El barber se'n va a casa");
    }
}
