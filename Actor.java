/* Interface Actor, Activitat: BarberDorment, José Sánchez Moreno 16/11/15 */

interface Actor {
    void decideix(int i);
    void accio();
}
